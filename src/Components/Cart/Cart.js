import React from 'react';
import './Cart.css';

import SelectedProduct from './Components/SelectedProducts';
import TotalPrice from './Components/TotalPrice'

class Cart extends React.Component {

  render() {
    return (
      <section className="user-cart">
        <h4>Twój koszyk</h4>
        <ul className="user-cart--list">
          <SelectedProduct/>
        </ul>
        <TotalPrice/>
      </section>
      )
  }
}

export default Cart;
