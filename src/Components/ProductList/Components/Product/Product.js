import React from 'react';
import './Product.css';

class Product extends React.Component {
  render() {
    return (
      <div className="product-box">
        <div>
          <img className="product-box--image" src="http://lorempixel.com/140/180/" />
        </div>
        <div>
          <h2>Nazwa produktu</h2>
          <pre>Cena: 12 039zł</pre>
          <p>Opis produktu. Lorem ipsum dolor sit amente.</p>
          <button>Dodaj do koszyka</button>
        </div>
      </div>
    )
  }
}


export default Product;
