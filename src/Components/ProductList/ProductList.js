import React from 'react';
import './ProductList.css';
import SearchBox from './Components/SearchBox';
import Product from './Components/Product'

class ProductList extends React.Component {
  render() {
    return (
      <section className="product-list">
        <SearchBox/>
        <Product />
        <Product />
        <Product />
        <Product />
      </section>
      )
  }
}
ProductList.defaultProps = {
  productList: []
}

export default ProductList;
